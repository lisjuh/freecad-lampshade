# FreeCAD Lampshade Round

This repository contains the CAD design files for a self-designed lampshade, created using FreeCAD. The lampshade is designed for lamps that don't have one and therefore cause glare.

![Lamp](./images/FreeCAD/Lamp.png "Lamp")

## Specifications

- Lampshade Dimensions:
  - top diameter: 90 mm
  - base diameter: 130 mm
  - height from base to top diameter is 182 mm
  - wall thickness is the nozzle size

- Ring Dimensions: 
  - outer diameter: 91 mm
  - inner diameter: 36 mm
  - thickness: 2 mm

- Lampshade Features:
  - Fits light bulb socket E27 (european) or ES (american)
  - Consists of two parts:
    1. Lampshade
    2. Ring (is for support, and is glued to the lampshade)

## Design Files

The CAD design files are provided in the following formats:

- `Lampshade.fcstd` (FreeCAD native format)
- `Lampshade.stl` (STereoLithography file format of the lampshade for 3D printing)
- `Lampshade_Ring.stl` (STereoLithography file format of the ring for 3D printing)

## Variants of the Design

- V1: Flower 

## 3D Printer Settings

### Lampshade

- vase/spiral mode with 0 base layers and 0 top layer
  * rafts: No
  * supports: No
  * infill: 0%
- layer height: 0.2mm
- 10 loop skirt printed in 2 layers
- filament material: PETG 
- printed with increased extrusion width in Vase mode: 0.8mm on 0.4 nozzle

### Ring

- nozzle: 0.4 mm
- layer height: 0.2 mm
- infill: 15%
- filament material: PLA


<!-- ## 3D Printed Straw Box

<img src="./images/ProductPhotos/StrawBox1.jpg" alt="3D printed Straw Box" width="1300" height="auto"> -->

## Contributing

If you have any suggestions, improvements, or modifications to the design, please feel free to submit a pull request or open an issue in this repository.


## License

This project is licensed under the [Creative Commons Attribution-NonCommercial 4.0 International](https://creativecommons.org/licenses/by-nc/4.0/deed.en) license.
